import React from "react"
import styled from "styled-components"
import {
  Header,
  Link,
  PageContainer,
  HeaderWithDate,
  TextContainer,
  TagList,
  List,
  ListItem,
} from "../components"
import { SmallSubtitleNoMargin, SmallerHeader } from "../components/Header"
import { PostLink } from "../components/PostLink"
import { TagLink } from "../components/Tags"
import "./blogTemplate.module.css"

const ArticleContainer = styled.div`
  padding-bottom: 2rem;
  width: 100%;
  @media screen and (min-width: 40em) {
    margin: auto;
    width: 70%;
  }
`
const Section = styled.section`
  padding: 1rem 2rem;
  margin: 2rem 0;
  font-size: 18px;
  font-family: "Merriweather", "Georgia", serif;
`
export default function Template({
  data, // this prop will be injected by the GraphQL query below.
}) {
  const { markdownRemark } = data // data.markdownRemark holds our post data
  const { frontmatter, html } = markdownRemark
  const tags = frontmatter && (frontmatter.tags || [])
  console.log(tags)
  const Tags = tags.map((tag, key) => (
    <ListItem key={key}>
      <TagLink tag={tag} />
    </ListItem>
  ))
  const { prev_post, next_post } = frontmatter
  return (
    <PageContainer>
      <SmallerHeader>
        <Link href="/" color="black">
          isthisblog{" "}
        </Link>
      </SmallerHeader>
      <ArticleContainer>
        <Section>
          <div>
            <HeaderWithDate title={frontmatter.title} date={frontmatter.date} />
            <TextContainer
              className="markdown-body"
              dangerouslySetInnerHTML={{ __html: html }}
            />
          </div>
          <hr />
          <TextContainer>
            <p>
              Email me your thoughts at{" "}
              <Link href="mailto:isthisnagee+blog@gmail.com">
                isthisnagee+blog@gmail.com
              </Link>
            </p>
            <p>-Nagee</p>
          </TextContainer>
        </Section>
        <div>
          <List>
            <ListItem>
              <PostLink {...next_post} type="next" />
            </ListItem>
            <ListItem>
              <PostLink {...prev_post} type="prev" />
            </ListItem>
          </List>
        </div>
      </ArticleContainer>
      <List>{Tags}</List>
    </PageContainer>
  )
}

export const pageQuery = graphql`
  query BlogPostByPath($path: String!) {
    markdownRemark(frontmatter: { path: { eq: $path } }) {
      html
      frontmatter {
        date(formatString: "MMMM DD, YYYY")
        path
        title
        tags
        prev_post {
          link_type
          name
          link
        }
        next_post {
          link_type
          name
          link
        }
      }
    }
  }
`
