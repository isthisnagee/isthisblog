import React from "react"
import PropTypes from "prop-types"

// Components
import {
  Header,
  Link,
  PageContainer,
  HeaderWithDate,
  TextContainer,
  TagList,
  BlogPostExcerpt,
} from "../components"
import { SmallSubtitleNoMargin, Subtitle } from "../components/Header"
import { RegularLink } from "../components/Link"
import {
  BlogPostList,
  BlogPostItem,
  BlogPostDescription,
  BlogPostNumber,
  BlogPostHeader,
} from "../components/BlogPostList"

const Tags = ({ pathContext, data }) => {
  const { tag } = pathContext
  const { edges, totalCount } = data.allMarkdownRemark
  const tagHeader = `${totalCount} post${
    totalCount === 1 ? "" : "s"
  } tagged with "${tag}"`

  return (
    <PageContainer>
      <div>
        <Header>
          <RegularLink href="/" color="black">
            isthisblog
          </RegularLink>
        </Header>
        <Subtitle>tag: {tag}</Subtitle>
        <Link href="/tags">All tags</Link>
        <BlogPostList>
          {edges
            .filter(edge => !!edge.node.frontmatter.date)
            .map(({ node }, k, arr) => (
              <BlogPostItem>
                <BlogPostNumber>{arr.length - k}</BlogPostNumber>
                <BlogPostHeader>
                  <Link href={node.frontmatter.path}>
                    {node.frontmatter.title}
                  </Link>
                </BlogPostHeader>
                <BlogPostDescription>{node.excerpt}</BlogPostDescription>
              </BlogPostItem>
            ))}
        </BlogPostList>
      </div>
    </PageContainer>
  )
}

Tags.propTypes = {
  pathContext: PropTypes.shape({
    tag: PropTypes.string.isRequired,
  }),
  data: PropTypes.shape({
    allMarkdownRemark: PropTypes.shape({
      totalCount: PropTypes.number.isRequired,
      edges: PropTypes.arrayOf(
        PropTypes.shape({
          node: PropTypes.shape({
            frontmatter: PropTypes.shape({
              path: PropTypes.string.isRequired,
              title: PropTypes.string.isRequired,
            }),
          }),
        }).isRequired
      ),
    }),
  }),
}

export default Tags

export const pageQuery = graphql`
  query TagPage($tag: String) {
    allMarkdownRemark(
      limit: 2000
      sort: { fields: [frontmatter___date], order: DESC }
      filter: { frontmatter: { tags: { in: [$tag] } } }
    ) {
      totalCount
      edges {
        node {
          id
          excerpt(pruneLength: 250)
          frontmatter {
            title
            path
            date(formatString: "MMMM DD, YYYY")
          }
        }
      }
    }
  }
`
