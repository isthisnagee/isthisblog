import React from 'react'
import styled from 'styled-components'
import Link from './Link'

const SpanNext = styled.span`
  margin-left: 2rem;
`
export const PostLink = ({link_type, name, link, type}) => {
  const msg = type === "prev" ? "Previous Post:" : "Next Post:"
  if (link_type === 'na') return false
  if (link_type === 'link') return <SpanNext>{msg} <Link href={link}>{name}</Link></SpanNext>
  if (link_type === 'string') return <SpanNext>{msg} {name}</SpanNext>
  console.log(type)
  console.log('nooo')
  return false
}
