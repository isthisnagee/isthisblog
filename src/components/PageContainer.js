import React from "react"
import styled from "styled-components"


const _PageContainer = styled.div`
  font-family: "Benton Sans", "Helvetica Neue", helvetica, arial, sans-serif;
  margin: 0;
  padding: 1rem;
  box-sizing: border-box;
  min-height: 100vh;
  *zoom: 1;
  display: flex;
  flex-wrap: wrap;

  @media screen and (min-width: 40em) {
    padding: 4rem;
  }
`

const Border = styled.div`
  width: 0;
  height: 0;
  position: fixed;
  background-color: pink;
  @media screen and (min-width: 40em) {
    ${({height}) => `height: ${height}`}
    ${({width}) => `width: ${width}`}
  }
`
const PageContainer = ({
  ...props
}) => <React.Fragment>
  <Border height="10px" width="100%" style={{ top: "0", }}/>
  <Border height="100%" width="10px" style={{ top: "0", left: "0", }}/>
  <Border height="100%" width="10px" style={{ top: "0", right: "0",}}/>
  <Border height="10px" width="100%" style={{ bottom: "0" }}/>
<_PageContainer {...props} />
</React.Fragment>
export const PageContent = styled.div`
  width: 100%;
  float: none;
  @media screen and (min-width: 30em) {
    width: 87%;
    float: left;
  }
`

export const PageSidebar = styled.div`
  width: 100%;
  float: none;
  height: 10vh;
  @media screen and (min-width: 40em) {
    width: 13%;
    float: left;
  }
`

export default PageContainer
