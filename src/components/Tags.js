import React from "react"
import styled from "styled-components"
import kebabCase from "lodash/fp/kebabCase"

import { RegularLink } from "./Link"

export const Tag = styled.div`
  display: inline-block;
  margin-right: 0.4rem;
  background-color: pink;
  color: black;
  box-shadow: 3px 3px pink;
  border: 1px solid black;
  letter-spacing: 1px;
  padding: 0 0.2rem;

  &::before {
    content: "#";
  }
`

export const Tags = styled.div`
  display: flex;
  flex-direction: horizontal;
`

const TagList = ({ tags }) => (
  <Tags>
    {tags.map((tag, key) => (
      <Tag key={key}>{tag}</Tag>
    ))}
  </Tags>
)

export const TagLink = ({ tag }) => (
  <Tag>
    <RegularLink href={`/tags/${kebabCase(tag)}`} color="black">
      {tag}
    </RegularLink>
  </Tag>
)

const TagListLink = ({ tags }) => (
  <Tags>
    {tags.map((tag, key) => (
      <TagLink tag={tag} key={key} />
    ))}
  </Tags>
)

export class FilteredTagList extends React.Component {
  constructor(props) {
    super(props)
    console.log(props.tags)
    this.state = {
      searchQuery: "",
      matchingTags: props.tags,
    }
  }

  search = e => {
    const {
      target: { value },
    } = e
    if (!value) {
      this.setState({
        searchQuery: "",
        matchingTags: this.props.tags,
      })
      return
    }

    this.setState({
      searchQuery: value,
      matchingTags: this.props.tags.filter(tag =>
        tag.toLowerCase().includes(value.toLowerCase())
      ),
    })
  }

  render() {
    const {
      search,
      state: { matchingTags },
    } = this
    return (
      <div>
        <input
          style={{ color: "white", marginBottom: "1.4rem" }}
          onChange={search}
        />
        <TagListLink tags={matchingTags} />
      </div>
    )
  }
}

export default TagListLink
