import styled from "styled-components"

export const BlogPostList = styled.div``

export const BlogPostItem = styled.section`
  display: grid;
  grid-template-rows: min-content min-content;
  grid-template-areas: "number" "header" "description";

  @media screen and (min-width: 30em) {
    grid-template-areas: "number header" ". description";
    grid-template-columns: 4rem repeat(auto-fit, minmax(50%, 1fr));
  }
`

export const BlogPostDescription = styled.div`
  grid-area: description;
  padding: 0;
  margin: 0;
  color: #888;
`

export const BlogPostNumber = styled.span`
  color: rgba(0, 0, 0, 0.8);
  margin-bottom: 0.2rem; grid-area: number; 
  text-align: center;
  @media screen and (min-width:30em) {
    display: flex; 
    justify-content: flex-end;
    flex-direction: column;
    align-items: center;
  }
}

`

export const BlogPostHeader = styled.h1`
  grid-area: header;
  margin-bottom: 0.2rem;
  text-decoration: underline;
`
