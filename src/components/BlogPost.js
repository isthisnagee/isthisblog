import React from "react"
import styled from "styled-components"

import { TextContainer } from "./TextContainer"
import { SmallerHeader } from "./Header"
import Link from "./Link"

export const BlogPostItem = styled.article`
  border: 3px solid black;
  box-shadow: 9px 9px black;
  padding: 1rem;
  margin-bottom: 1rem;
  margin-right: 1rem;
  display: flex;
  flex-direction: column;
  width: 30em;
  @media screen and (min-width: 30em) {
    height: 300px;
  }
`

export const BlogPostDate = styled.h2`
  font-size: 0.7rem;
`

const Text = styled.div`
  flex: 1;
  overflow: none;
`

const Footer = styled.div``
const Header = Footer
const BlogPostItemWithExcerpt = ({ title, excerpt, path, date }) => (
  <BlogPostItem>
    <Header>
      <SmallerHeader>
        <Link href={path}>{title}</Link>
      </SmallerHeader>
      <BlogPostDate>{date}</BlogPostDate>
    </Header>
    <Text>{excerpt}</Text>
    <Footer>
      <Link style={{ float: "right" }} href={path}>
        →
      </Link>
    </Footer>
  </BlogPostItem>
)

export default BlogPostItemWithExcerpt
