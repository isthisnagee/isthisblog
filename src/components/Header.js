import React from "react"
import styled from "styled-components"

const Header = styled.h1`
  font-size: 3rem;
  font-family: "Merriweather", "Georgia", serif;
  margin: 1rem 0;
`

export const SmallerHeader = styled.h1`
  font-family: "Merriweather", "Georgia", serif;
  font-size: 1.2rem;
  color: pink;
`

export const HeaderWithNoMargin = styled.h1`
  font-family: sans-serif;
  font-size: 2.2rem;
  margin-bottom: 0;
`

export const Subtitle = styled.h2`
  font-family: sans-serif;
  font-size: 1.8rem;
`

export const SmallSubtitleNoMargin = styled.h2`
  color: black;
  opacity: 0.8;
  font-family: sans-serif;
  font-size: 0.8rem;
  margin-top: 0;
`

export const HeaderWithDate = ({ title, date }) => (
  <div>
    <HeaderWithNoMargin>{title}</HeaderWithNoMargin>
    <SmallSubtitleNoMargin>{date}</SmallSubtitleNoMargin>
  </div>
)

export default Header
