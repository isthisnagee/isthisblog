import React from "react"
import styled from "styled-components"

const Link = styled.a`
  color: #e395ae;
  text-decoration: underline;
`

export const RegularLink = styled.a`
  color: ${props => props.color || "pink"};
  text-decoration: none;
`

export default Link
