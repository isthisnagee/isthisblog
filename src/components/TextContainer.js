import React from "react";
import styled from "styled-components";

export const TextContainer = styled.article`
  max-width: 33rem;
  line-height: 1.5;
`
