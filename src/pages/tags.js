import React from "react"
import PropTypes from "prop-types"

// Utilities
import kebabCase from "lodash/kebabCase"

// Components
import {
  TagList,
  PageContainer,
  Header,
  BlogPostExcerpt,
  Link,
} from "../components"
import { Subtitle, SmallSubtitleNoMargin } from "../components/Header"
import { FilteredTagList } from "../components/Tags"
import { RegularLink } from "../components/Link"

const TagsPage = ({
  data: {
    allMarkdownRemark: { group },
  },
}) => (
  <PageContainer>
    <div>
      <Header>
        <Link href="/" color="black">
          isthisblog
        </Link>
      </Header>
      <Subtitle>tags</Subtitle>
      <FilteredTagList tags={group.map(tag => tag.fieldValue)} />
    </div>
  </PageContainer>
)

TagsPage.propTypes = {
  data: PropTypes.shape({
    allMarkdownRemark: PropTypes.shape({
      group: PropTypes.arrayOf(
        PropTypes.shape({
          fieldValue: PropTypes.string.isRequired,
          totalCount: PropTypes.number.isRequired,
        }).isRequired
      ),
    }),
  }),
}

export default TagsPage

export const pageQuery = graphql`
  query TagsQuery {
    allMarkdownRemark(limit: 2000) {
      group(field: frontmatter___tags) {
        fieldValue
        totalCount
      }
    }
  }
`
