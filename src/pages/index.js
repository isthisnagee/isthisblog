import React from "react"
import {
  List,
  ListItem,
  PageContainer,
  Header,
  BlogPostExcerpt,
  Link,
} from "../components"
import { SmallSubtitleNoMargin } from "../components/Header"
import { PageContent, PageSidebar } from "../components/PageContainer"
import { TagLink } from "../components/Tags"
import {
  BlogPostList,
  BlogPostItem,
  BlogPostDescription,
  BlogPostNumber,
  BlogPostHeader,
} from "../components/BlogPostList"

const IndexPage = ({
  data: {
    allMarkdownRemark: { edges, group },
  },
}) => {
  const r = () => Math.floor(Math.random() * 20) + 1
  const Posts = edges
    .filter(edge => !!edge.node.frontmatter.date)
    .map(({ node }, k, arr) => (
      <BlogPostItem>
        <BlogPostNumber>{arr.length - k}</BlogPostNumber>
        <BlogPostHeader>
          <Link href={node.frontmatter.path}>{node.frontmatter.title}</Link>
        </BlogPostHeader>
        <BlogPostDescription>
          {node.excerpt.substring(
            0,
            Math.min(40 + r(), node.excerpt.length - 1)
          )}
          ...
        </BlogPostDescription>
      </BlogPostItem>
    ))

  const Tags = group.map(({ fieldValue: tag }, key) => (
    <ListItem key={key}>
      <TagLink tag={tag} />
    </ListItem>
  ))

  return (
    <PageContainer>
      <PageContent>
        <Header>isthisblog</Header>
        <SmallSubtitleNoMargin>
          See{" "}
          <Link href="https://links.isthisblog.com">links.isthisblog.com</Link>{" "}
          for link aggregation posts.
        </SmallSubtitleNoMargin>
        <SmallSubtitleNoMargin>
          by <Link href="https://twitter.com/@isthisnagee">@isthisnagee</Link>
        </SmallSubtitleNoMargin>
        <BlogPostList>{Posts}</BlogPostList>
      </PageContent>
      <PageSidebar>
        <List>{Tags}</List>
      </PageSidebar>
    </PageContainer>
  )
}

export default IndexPage

export const pageQuery = graphql`
  query IndexQuery {
    allMarkdownRemark(sort: { order: DESC, fields: [frontmatter___date] }) {
      edges {
        node {
          id
          excerpt(pruneLength: 250)
          frontmatter {
            date(formatString: "MMMM DD, YYYY")
            path
            title
          }
        }
      }
      group(field: frontmatter___tags) {
        fieldValue
        totalCount
      }
    }
  }
`
