---
path: /blog/series/digital-text-01
date: '2018-07-07'
title: 'The Digital Text 01: Understanding Media'
tags:
  - series
  - digital-text
  - opinion
  - readings
next_post:
  link: Coming Soon!
  name: Coming Soon!
  link_type: string
prev_post:
  link_type: na
---
Currently reading some select chapters from _Understanding Media: The Extensions of Man_ by Marshall McCluhan. Here are some thoughts, quotes, and key points on it:

## The Content of all medium is another medium

The content of writing is speech. The content of Print is the written word, the content of the telegraph is the written word.

This makes me wonder what the initial medium is, I think a graph of these things would be really cool. 

Let's take "digital text" (like this blog post for example)

```
This Blog -> Print -> Written word -> Speech -> Thoughts, Impluses? -> ?
```

## The Message of a Medium

Take the current pace or scale or pattern within any facet of human affairs. Then the message of a medium is the change of scale or pace or pattern in human affairs.

So the movie changes the scale of speed and time. Let's take the graph above and play with it.

![content and message of media](/assets/content-and-message-of-medium.png)

## Light

There's a big emphasis on the light that I'm still struggling to understand. Something about how we only notice the content of the light: the other medium.
