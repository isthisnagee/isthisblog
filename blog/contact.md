---
path: /blog/contact-link
date: '2018-06-22'
title: Contact
tags:
  - opinion
  - ux
next_post:
  link_type: na
prev_post:
  link_type: na
---
I love viewing professional portfolios online: I get to see the work people put effort into, sometimes in their free time!
The really special treats are the ones that aren't using a template (or an easily recognizable template 😛); regardless of
the design, it's so much fun seeing what people do. These sites generally contain sections a section for work, an "About Me" link, and a "Contact" link.

I also really like going to "About" and "Contact Us" pages for startups. The about pages give me a sense of their culture and
 brand, and the "Contact Us" page sometimes shows me their presense on social media. 

The "Contact" link is always a risky click. I've seen two approaches on the contents of that link:

1. Clicking opens a different part of their website with links to social media, like twitter, linkedin, email, and others.
2. Clicking opens my email client.

The second option bothers me _a lot_. I think it's very user hostile and it removes me from that moment where I was engaged. In the context of these pages,
I don't think anybody wants users to leave their site unless the action expliclitiy indicates so.

I realize that a potential customer or job or connection might be reached through clicking that email opening contact button, but I think when the action is unexpected, the immediate response is to close the email client.

I've made a pact to myself that any `mailto` link should be made known to the user through the link text.
Some examples:

- `<a href="mailto:isthisnagee@gmail.com">email<a>`
- `<a href="mailto:isthisnagee@gmail.com">isthisnagee@gmail.com<a>`

The "Contact" link is critical for some people, it's the reason why people put it near their site's navbar, and users should not be turned away because of a "gotcha".

I wonder if there's a better way to indicate that the "Contact" link behaves differently while making it consistent with the other links on the nav. Icons?
