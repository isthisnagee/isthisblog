---
path: /blog/poem-internet-coc
date: '2018-07-16'
title: An Internet CoC
tags:
  - internet-saves
  - poetry
next_post:
  link_type: na
prev_post:
  link_type: na
---
I came across [this](https://twitter.com/brainpicker/status/1018660751354073090) tweet today and had to save it somewhere, so I figured my blog would do haha.

I've screenshotted it just in case.



![a tweet stating that poem 22 by Emily Dickinson should be used as the internet's code of conduct](/assets/screen-shot-2018-07-16-at-12.05.11-am.png)

```
             LIFE
             XXII
I had no time to hate, because	
The grave would hinder me,	
And life was not so ample I	
Could finish enmity.	
  
Nor had I time to love; but since
Some industry must be,	
The little toil of love, I thought,	
Was large enough for me.

- Emily Dickinson
```
