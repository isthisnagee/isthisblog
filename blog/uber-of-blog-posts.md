---
path: /blog/uber-of-blog-posts
date: '2018-06-21'
title: Uber of Blog Posts
tags:
  - opinion
next_post:
  link_type: na
prev_post:
  link_type: na
---
"Think of us as the Uber of {{industry}}". 



What does that even mean? Like it actually makes no sense to me. What qualities do you have that make you uber-like, and are those qualities even unique to uber? What? It seems lazy and unimaginative—you described all your hard work as the "Uber of" whatever your industry is.


Let's take some things about Uber that may or may not be true but somehow happen to be part of _my_ impression of Uber. This is what I think of when you say you're the "Uber of" whatever industry you're in:


- You have a huge problem with sexual harrasments.

- You are automating some part of {{industry}} and are partnered with a bunch of universities funding research
- You have solid infrastructure and are known for it.

- \#DeleteUber

- I'd rather pay a little bit more and take a cab in downtown Toronto, it's faster.

- People have compared you to ISIS (this is an idiotic item to put and 
    I don't like adding it but it did become a meme in Toronto)

- I like your main competitor a little bit more.

- You are operating at a loss 

- You are promising huge returns \_one day\_. 

- You treat part of your customer base like garbage 

- You can operate at huge scale and quickly react to market demands

- You have a bunch of funding.

- Your founder is an idiot.

- You constantly have issues with governments.

- People burn out quick.

I asked a friend what they think it means to be "Uber of {{indsutry}}". They said "means a software solution for a physical demand that doesn’t currently have a software solution."

Maybe it's "a software solution that brings the physical product to you"? Probably not.

Please for stop using "Uber of {{industry}}". It's lazy, it's generic, it comes with baggage, and it doesn't mean anything unless you explicitly define what it is you mean (and if you've done that, might as well remove your comparison to
Uber).


> Anything that's the "something" of the "something" isn't really the "anything"
> of "anything"
> - Lisa Simpson



I've done things before where I've said "It's like {{company}} of
{{industry}}", and I'm trying my best not to do that anymore. Maybe sometimes it's the easiest way to make a connection?!  Like I have a vague understanding of what people mean when they say they're the Uber of something—that they're revolutionizing their industry, but everybody thinks they're doing that.


I'd love to hear what your ideas on what it means to be "Uber of {{industry}}".
