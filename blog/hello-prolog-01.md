---
path: /blog/series/hello-prolog/00
date: '2018-06-23'
title: 'Hello Prolog: 00'
tags:
  - series
  - hello-prolog
  - programming
next_post:
  link: /blog/series/hello-prolog/01
  name: 'Data Structures, Operation, and Control'
  link_type: link
prev_post:
  link: ''
  name: ''
  link_type: na
---
Hi everybody! This is the first article in a series of articles about the programming language Prolog!

A while back I randomly went bookshopping and picked up [Prolog for Programmers](https://books.google.ca/books/about/Prolog_for_Programmers.html?id=IYQZAQAAIAAJ&redir_esc=y&hl=en) by Feliks Kluźniak and Stanislław Szpakowicz for around $10.



!["Prolog for Programmers" book cover](/assets/p4p_cover.png)

In total there will be 9 posts including this one. I've decided to write one post for each of the 9 chapters, except for the last chapter on Prolog Dialects. I might end up writing about it and forget to amend this, if you notice this, please send me an email (see the end of the post for my email).
