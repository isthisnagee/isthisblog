# isthisblog

My blog :D
https://isthisblog.com

# Infrastructure 

- GatsbyJS
- NetlifyCMS
- ReactJS
- StyleComponents


# TODO: 

- [ ] Add "Series" to netlify CMS https://github.com/isthisnagee/isthisblog/issues/1
- [ ] Add rss feed https://github.com/isthisnagee/isthisblog/issues/2.
- [ ] Add Subscibe button for series https://github.com/isthisnagee/isthisblog/issues/3.
